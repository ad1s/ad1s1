all: fichierz5 fichierJS

fichierz5:
	@outils/inform-633/src/inform '$$MAX_STATIC_DATA=30000' '$$MAX_LABELS=12000' -s~S -e -f +include_path=,bibliotheques +Language_name=French AD1S1.inf

fichierJS:
	python outils/game2js.py AD1S1.z5 >interpreter/test.z5.js
	
test:
	@outils/inform-633/src/inform '$$MAX_STATIC_DATA=30000' '$$MAX_LABELS=12000' -v8 -X +include_path=,bibliotheques +Language_name=French AD1S1.inf