Les Aventures d'Un Soir - Épisode 1
====================================


  Voici le jeu créé par les participants à la première édition des
Aventures d'Un Soir. Amusez-vous bien !


  Pad de travail : https://huit.re/AD1S1

  Retrouvez-nous sur https://ad1s.itch.io/

  Ce jeu a été créé grâce à la participation de :
  * aleximagine
  * Hellimary
  * hlabrande
  * mtrebitsch
  * stephaneflauder
  * Ulukos
  * vince_v1
  * Yakkafo

  Crédits sons:
  Place du marché: https://freesound.org/people/Metzik/sounds/371222/
  Docks: http://freesound.org/people/fortom/sounds/274604/
  Taverne: http://freesound.org/people/lonemonk/sounds/167923/